package cl.alaya.poc.google.maps;


import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;
import java.io.StringWriter;

@ManagedBean(name = "googleMapsBean")
public class GoogleMapsBean {

    private String direccion;
    private String cordenadas;
    private String latitud;
    private String longitud;
    private String respuestaServicio;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCordenadas() {

        try{
            if(this.direccion != null && !"".equals(this.direccion.trim())){

                String direccionConsulta = direccion.trim().replaceAll(" +", "+");
                System.out.println("Direccion Consulta: " + direccionConsulta);

                HttpPost httpPost = new HttpPost("http://maps.googleapis.com/maps/api/geocode/xml?address=" + direccionConsulta + "&sensor=true");
                CloseableHttpClient httpclient = HttpClients.createDefault();
                CloseableHttpResponse closeableHttpResponse = httpclient.execute(httpPost);
                System.out.println("Status: " + closeableHttpResponse.getStatusLine());
                HttpEntity httpEntity = closeableHttpResponse.getEntity();
                InputStream isContent = httpEntity.getContent();

                InputSource geocoderResultInputSource = new InputSource(isContent);
                Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(geocoderResultInputSource);

                this.respuestaServicio = this.documentToString(document);


                XPath xpath = XPathFactory.newInstance().newXPath();

                NodeList resultNodeListLatitud = (NodeList) xpath.evaluate("/GeocodeResponse/result/geometry/location/lat", document, XPathConstants.NODESET);

                for (int i = 0; i < resultNodeListLatitud.getLength(); ++i) {
                    this.latitud = resultNodeListLatitud.item(0).getTextContent();
                    break;
                }

                NodeList resultNodeListLongitud = (NodeList) xpath.evaluate("/GeocodeResponse/result/geometry/location/lng", document, XPathConstants.NODESET);

                for (int i = 0; i < resultNodeListLongitud.getLength(); ++i) {
                    this.longitud = resultNodeListLongitud.item(0).getTextContent();
                    break;
                }

                if(this.latitud != null && !"".equals(this.latitud) && this.longitud != null && !"".equals(this.longitud) ){
                    this.cordenadas ="Cordenadas: " + this.latitud + ";" + this.longitud;
                }

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Consulta Exitosa", "La consulta de cordenadas se ha realizado con exito."));


            }else{

                this.cordenadas = "";
            }


        } catch (Exception e){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se produjo un error al realizar la consulta"));
            this.cordenadas = "";
        }

        return cordenadas;
    }

    public void setCordenadas(String cordenadas) {
        this.cordenadas = cordenadas;
    }

    public String getRespuestaServicio() {
        return respuestaServicio;
    }

    public void setRespuestaServicio(String respuestaServicio) {
        this.respuestaServicio = respuestaServicio;
    }



    public static String documentToString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}

