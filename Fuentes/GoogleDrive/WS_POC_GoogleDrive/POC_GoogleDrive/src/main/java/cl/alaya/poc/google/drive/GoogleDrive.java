package cl.alaya.poc.google.drive;

import org.primefaces.event.FileUploadEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@ManagedBean(name = "googleDriveBean")
public class GoogleDrive {

    public void handleFileUpload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try{

            String pathDirTmp = "C:/archivodDrive/";
            File dir = new File(pathDirTmp);
            if (!dir.exists()) {
                dir.mkdir();
            }

            InputStream is = event.getFile().getInputstream();
            File file = new File(pathDirTmp + event.getFile().getFileName());
            OutputStream os = new FileOutputStream(file);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = is.read(bytes)) != -1) {
                os.write(bytes, 0, read);
            }

            DriveUpload driveUpload = new DriveUpload();
            driveUpload.subirArchivo(file);
            context.addMessage(null, new FacesMessage("El archivo " + event.getFile().getFileName() + " se ha cargado correctamente." ));

        }catch (Exception e){
            e.printStackTrace();
            context.addMessage(null, new FacesMessage("Se produjo un error al intentar cargar el archivo."));
        }


    }
}
